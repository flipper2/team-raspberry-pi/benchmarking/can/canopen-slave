import canopen
import time
import logging

#logging.basicConfig(level=logging.DEBUG)

network = canopen.Network()
network.connect(bustype='socketcan', channel='can0')
network.check()

node = canopen.LocalNode(35, 'eds/e35.eds')

network.add_node(node)

node.nmt.state = 'OPERATIONAL'
node.rpdo.read()
node.tpdo.read()

def on_receive(map):
	raw = node.rpdo["Target velocity"].raw

	# bump by one and send back
	node.tpdo[1]['Velocity actual value'].raw = raw + 1
	node.tpdo[1].transmit()

node.rpdo[1].add_callback(on_receive)

try:
	time.sleep(99999)
finally:
	network.disconnect()
